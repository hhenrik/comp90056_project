A Basic How to Run in Eclipse

1. System Requirements
- JRE version 7
- Eclipse IDE

2. Download the source code as .zip and extract to a local fold1er

3. Start Eclipse, then import project, pointing the directory to the source code

4. Go to COMP90056_Project -> src/jvm -> CDRproject -> CDRTopology.java 

5. In line 45 (that is, String fileName = "G:\\cdr.rawsample2.txt";), change the file path based on where it locates 

6. data rate can be changed in line 43 (double rate = 10000;). A data rate of 0 has no restriction on the number of tuples processed

7. (Optional) switch debug mode in line 42

8. Finally, run CDRTopology.java as Java Application


