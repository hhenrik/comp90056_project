import java.util.concurrent.ConcurrentHashMap;


/**
 * CDRRecord class
 * 
 * @author Henrik
 */
public class CDRRecord
{
	/*
	 * Attributes
	 */
<<<<<<< HEAD
	final private String MTC = "MTC";
	final private String MOT = "MOC";
	final private String cdrType = "CDRTYPE";
	final private String imsi = "IMSI";
	final private String imei = "IMEI";
	final private String callingNumber = "CALLINGNUMBER";
	final private String calledNumber = "CALLEDNUMBER";
	final private String recordingEntity = "RECORDINGENTITY";
	final private String location = "LOCATION";
	final private String callReference = "CALLREFERENCE";
	final private String callDuration = "CALLDURATION";
	final private String answerTime = "ANSWERTIME";
	final private String seizureTime = "SEIZURETIME";
	final private String releaseTime = "RELEASETIME";
	final private String causeForTermination = "CAUSEFORTERMINATION";
	final private String basicService = "BASICSERVICE";
	final private String mscAddress = "MSCADDRESS";
	
	final private String[] CDRFIELDS = {cdrType, imsi, imei, callingNumber, calledNumber, 
			                            recordingEntity, location, callReference, callDuration,
			                            answerTime, seizureTime, releaseTime, causeForTermination,
			                            basicService, mscAddress};
	private ConcurrentHashMap<String, String> record;
	
=======
	final public String							MTC					= "MTC";
	final public String							MOT					= "MOC";
	final private String						cdrType				= "CDRTYPE";
	final private String						imsi				= "IMSI";
	final private String						imei				= "IMEI";
	final private String						callingNumber		= "CALLINGNUMBER";
	final private String						calledNumber		= "CALLEDNUMBER";
	final private String						recordingEntity		= "RECORDINGENTITY";
	final private String						location			= "LOCATION";
	final private String						callReference		= "CALLREFERENCE";
	final private String						callDuration		= "CALLDURATION";
	final private String						answerTime			= "ANSWERTIME";
	final private String						seizureTime			= "SEIZURETIME";
	final private String						releaseTime			= "RELEASETIME";
	final private String						causeForTermination	= "CAUSEFORTERMINATION";
	final private String						basicService		= "BASICSERVICE";
	final private String						mscAddress			= "MSCADDRESS";

	final private String[]						CDRFIELDS			= {
			cdrType,
			imsi,
			imei,
			callingNumber,
			calledNumber,
			recordingEntity,
			location,
			callReference,
			callDuration,
			answerTime,
			seizureTime,
			releaseTime,
			causeForTermination,
			basicService,
			mscAddress												};
	private ConcurrentHashMap<String, String>	record;


>>>>>>> 016e362b2afd9259ba7144e2efa1a39bea7cf105
	/*
	 * Constructor
	 */
	CDRRecord (String str)
	{
		record = new ConcurrentHashMap<String, String> ();
		String[] attr = str.split (",");

		if (attr.length == CDRFIELDS.length)
		{
			for (int i = 0; i < CDRFIELDS.length; i++)
			{
				record.put (CDRFIELDS[i], attr[i].replaceAll ("\"", ""));
			}
		}
		else
		{
			System.out.println ("CDRRecord fields not match");
		}
	}


	public void printRecord()
	{
		for (String i : CDRFIELDS)
		{
			System.out.println (i + " " + record.get (i));
		}
	}


	/**
	 * Test
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		String str = "\"MOC\",\"301007645565635\",\"017881044129250\",\"18016754110\",\"12282120300\",\"000006623839681\",12879,1228,2171.2,\"2013-09-10 07:46:44\",\"\",\"2013-09-10 08:22:55\",\"00\",\"11\",\"000006623839681\"";
		CDRRecord newRecord = new CDRRecord (str);
		newRecord.printRecord ();
	}

}
