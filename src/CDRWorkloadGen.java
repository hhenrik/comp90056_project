import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * CDRWorkloadGen reads data file and simulates as a data stream with different rates.
 * - This is needed for testing and performance analysis of application
 * 
 * @author Henrik
 */
public class CDRWorkloadGen
{

	/** Encoding of file. Assumes it is StandardCharsets.ISO_8859_1 */
	protected final static Charset	ENCODING	= StandardCharsets.ISO_8859_1;

	private long					rate;
	private BufferedReader			reader;


	public CDRWorkloadGen (String fileName, long rate)
	{
		File f = new File (fileName);

		Path path = Paths.get (fileName);

		this.rate = rate;
		try
		{
			reader = Files.newBufferedReader (path, ENCODING);
			String line;
			while ((line = reader.readLine ()) != null)
			{
				processLine (line);
				Thread.sleep (rate);
			}
			reader.close ();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace ();
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace ();
		}

	}


	/**
	 * Overwrite this method for processing line
	 * - Print line for testing
	 * 
	 * @param line
	 */
	public void processLine(String line)
	{
		System.out.println (line);
	}


	/**
	 * Test
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		CDRWorkloadGen test = new CDRWorkloadGen ("cdr_rawsample1.txt", 3000);
	}
}
