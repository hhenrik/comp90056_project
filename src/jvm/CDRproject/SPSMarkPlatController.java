package CDRproject;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;

import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;

import java.awt.TextField;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.ScrollPaneConstants;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import java.awt.Color;
import javax.swing.JInternalFrame;
import javax.swing.JCheckBox;
import javax.swing.border.MatteBorder;
import javax.swing.border.LineBorder;

public class SPSMarkPlatController implements IRichSpout{

	JFrame frame;
	
	private static final long		serialVersionUID	= 1L;
	
	public final static Fields		fields				= new Fields ("cdrRule");
	protected SpoutOutputCollector	collector;
	
	/* attributes */
	//private SPSMarkPlatTemp refToStreamapp = null;
	private ArrayList<RuleSegment> rss = null;
	private RuleSegment rs = null;
	
	/*                JComponents                  */
	/* Text Fields */
	private JTextField T1_valueField1=null;
	private JTextField T1_valueField2=null;
	private JTextField T2_valueField1=null;
	private JTextField T2_valueField2=null;
	private JTextField T3_valueField2=null;
	
	/* CheckBox */
	private JCheckBox cbTemplate1 = null;
	private JCheckBox cbTemplate2 = null;
	private JCheckBox cbTemplate3 = null;
	/* JComboBox */
	private JComboBox T1_timeFrames = null;
	private JComboBox T1_promotions = null;
	private JComboBox T1_calltypes = null;
	
	private JComboBox T2_timeFrames = null;
	private JComboBox T2_promotions = null;
	
	private JComboBox T3_timeFrames = null;
	private JComboBox T3_promotions = null;

	/* Scroll List */
	private DefaultListModel listModel = null;
	private JList list = null;
	private JScrollPane scrollPane = null; 
	private JTextField textField;
	private JTextField textField_1;

	
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		
//	}

	/**
	 * Create the application.
	 */
	public SPSMarkPlatController() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		/* INITIALIZE ATTRIBUTES */
		rss = new ArrayList<RuleSegment>();
		rs = new RuleSegment();
		//refToStreamapp = new SPSMarkPlat();
		
		
		/* GUI INITIALISATION */
		listModel = new DefaultListModel();
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		
		frame.setBounds(100, 100, 723, 481);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(10, 101, 684, 103);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		String[] tempOpts = rs.TEMPLATES;
		
		String[] timeOpts = rs.TIMEFRAMES;
		T1_timeFrames = new JComboBox(timeOpts);
		T1_timeFrames.setBounds(597, 36, 65, 23);
		panel_2.add(T1_timeFrames);
		
		JLabel lblParameterSetting = new JLabel("Parameter Setting");
		lblParameterSetting.setBounds(10, 11, 112, 14);
		panel_2.add(lblParameterSetting);
		
		String[] opOpts = rs.OPERATIONS;
		
		JLabel lblRule = new JLabel("Rule: User makes more than");
		lblRule.setBounds(10, 36, 189, 23);
		panel_2.add(lblRule);
		
		JLabel lblNewLabel = new JLabel("  calls within");
		lblNewLabel.setBounds(408, 36, 116, 23);
		panel_2.add(lblNewLabel);
		
		T1_valueField1 = new JTextField();
		T1_valueField1.setBounds(209, 36, 65, 23);
		panel_2.add(T1_valueField1);
		T1_valueField1.setColumns(10);
		
		T1_valueField2 = new JTextField();
		T1_valueField2.setBounds(534, 36, 65, 23);
		panel_2.add(T1_valueField2);
		T1_valueField2.setColumns(10);
		
		JLabel lblPromotion = new JLabel("Promotion: ");
		lblPromotion.setBounds(10, 70, 112, 23);
		panel_2.add(lblPromotion);
		
		String[] promoOpts = rs.PROMOTIONS;
		T1_promotions = new JComboBox(promoOpts);
		T1_promotions.setBounds(116, 70, 341, 23);
		panel_2.add(T1_promotions);
		
		String[] callOpts = rs.CALLTYPES;
		T1_calltypes = new JComboBox(callOpts);
		T1_calltypes.setBounds(275, 36, 123, 23);
		panel_2.add(T1_calltypes);
		
		cbTemplate1 = new JCheckBox("Template_1");
		cbTemplate1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				cbTemplate2.setSelected(false);
				cbTemplate3.setSelected(false);
			}
		});
		cbTemplate1.setBounds(478, 7, 97, 23);
		panel_2.add(cbTemplate1);
		
		JLabel lblActiveRules = new JLabel("Active Rules: ");
		lblActiveRules.setBounds(10, 5, 84, 31);
		frame.getContentPane().add(lblActiveRules);
		
		list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setSize(523, 59);
		scrollPane.setLocation(10, 31);
		scrollPane.setViewportView(list);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		frame.getContentPane().add(scrollPane);
		
		JButton btnNewButton = new JButton("Activate");
		btnNewButton.setBounds(543, 29, 151, 31);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Deactivate");
		btnNewButton_1.setBounds(543, 59, 151, 31);
		frame.getContentPane().add(btnNewButton_1);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 215, 684, 103);
		frame.getContentPane().add(panel);
		
		T2_timeFrames = new JComboBox(timeOpts);
		T2_timeFrames.setBounds(598, 36, 65, 23);
		panel.add(T2_timeFrames);
		
		JLabel label = new JLabel("Parameter Setting");
		label.setBounds(10, 11, 112, 14);
		panel.add(label);
		
		JLabel lblRuleUserReceives = new JLabel("Rule: User receives more than");
		lblRuleUserReceives.setBounds(10, 36, 209, 23);
		panel.add(lblRuleUserReceives);
		
		JLabel lblAbnormalCallTerminations = new JLabel("abnormal call terminations within");
		lblAbnormalCallTerminations.setBounds(315, 36, 220, 23);
		panel.add(lblAbnormalCallTerminations);
		
		T2_valueField1 = new JTextField();
		T2_valueField1.setColumns(10);
		T2_valueField1.setBounds(211, 36, 72, 23);
		panel.add(T2_valueField1);
		
		T2_valueField2 = new JTextField();
		T2_valueField2.setColumns(10);
		T2_valueField2.setBounds(534, 36, 65, 23);
		panel.add(T2_valueField2);
		
		JLabel label_3 = new JLabel("Promotion: ");
		label_3.setBounds(10, 70, 112, 23);
		panel.add(label_3);
		
		T2_promotions = new JComboBox(promoOpts);
		T2_promotions.setBounds(116, 70, 341, 23);
		panel.add(T2_promotions);
		
		cbTemplate2 = new JCheckBox("Template_2");
		cbTemplate2.setBounds(478, 7, 97, 23);
		cbTemplate2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				cbTemplate1.setSelected(false);
				cbTemplate3.setSelected(false);
			}
		});
		panel.add(cbTemplate2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel_1.setLayout(null);
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(10, 329, 684, 103);
		frame.getContentPane().add(panel_1);
		
		T3_timeFrames = new JComboBox(rs.TIMEFRAMES);
		T3_timeFrames.setBounds(478, 36, 65, 23);
		panel_1.add(T3_timeFrames);
		
		JLabel label_1 = new JLabel("Parameter Setting");
		label_1.setBounds(10, 11, 112, 14);
		panel_1.add(label_1);
		
		JLabel lblRuleTop = new JLabel("Rule: Top 10 users who make the most number of outgoing calls  within");
		lblRuleTop.setBounds(10, 36, 352, 23);
		panel_1.add(lblRuleTop);
		
		JLabel label_5 = new JLabel("Promotion: ");
		label_5.setBounds(10, 70, 112, 23);
		panel_1.add(label_5);
		
		T3_promotions = new JComboBox(rs.PROMOTIONS);
		T3_promotions.setBounds(116, 70, 341, 23);
		panel_1.add(T3_promotions);
		
		cbTemplate3 = new JCheckBox("Template_3");
		cbTemplate3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				cbTemplate1.setSelected(false);
				cbTemplate2.setSelected(false);
			}
		});
		cbTemplate3.setBackground(Color.WHITE);
		cbTemplate3.setBounds(478, 7, 97, 23);
		panel_1.add(cbTemplate3);
		
		T3_valueField2 = new JTextField();
		T3_valueField2.setColumns(10);
		T3_valueField2.setBounds(414, 36, 65, 23);
		panel_1.add(T3_valueField2);
		
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				System.out.println("Test Deactivate Button");
				deactivateRuleSegment();
			}
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				System.out.println("Test Activate Button");
				activateRuleSegment();
			}
		});
		
		
	}
	
	/**
	 * Method to process rule from GUI controller
	 */
	public void activateRuleSegment(){
		
		// construct the new rule
		rs = new RuleSegment();
		// TEMPLATE 1 is selected
		if (cbTemplate1.isSelected()){
			rs.setTemplate(1);
			rs.setTimeframe(T1_timeFrames.getSelectedItem().toString());
			rs.setPromo(T1_promotions.getSelectedItem().toString());
			try{
				rs.setValue1(Integer.parseInt(T1_valueField1.getText()));
				rs.setValue2(Integer.parseInt(T1_valueField2.getText()));
			}catch(Exception e){
				rs.setValue1(0);
				rs.setValue2(0);
			}
			rs.setCallType(T1_calltypes.getSelectedItem().toString());
		}
		// TEMPLATE 2 is selected
		if (cbTemplate2.isSelected()){
			rs.setTemplate(2);
			try{
				rs.setValue1(Integer.parseInt(T2_valueField1.getText()));
				rs.setValue2(Integer.parseInt(T2_valueField1.getText()));
			}catch(Exception e){
				rs.setValue1(0);
				rs.setValue2(0);
			}
			rs.setTimeframe(T2_timeFrames.getSelectedItem().toString());
			rs.setPromo(T2_promotions.getSelectedItem().toString());
		}
		
		// TEMPLATE 3 is selected
		if (cbTemplate3.isSelected()){
			rs.setTemplate(3);
			try{
				rs.setValue2(Integer.parseInt(T3_valueField2.getText()));
			}catch(Exception e){
				rs.setValue2(0);
			}
			rs.setTimeframe(T3_timeFrames.getSelectedItem().toString());
			rs.setPromo(T3_promotions.getSelectedItem().toString());
		}
		//System.out.println(rs.toString());
		
		// store this rule, if it doesnt exist
		if ( ! rss.contains(rs) ){
			for (int i = 0; i<rss.size() ; i++){
				if (rss.get(i).getTemplate() == rs.getTemplate()){
					listModel.removeElement(rss.get(i));
					rss.remove(rss.get(i));
				}
			}
			rss.add(rs);
			// update active rule list
			listModel.addElement(rs);
			//System.out.println(rss.size());
			// notify Stream process
			//refToStreamapp.activateRule(rs.toString());
			processRule(rs);
		}
	}
	
	/**
	 * Method to deactivate rule from GUI controller
	 */
	public void deactivateRuleSegment(){
		int index = list.getSelectedIndex();
		RuleSegment rs = rss.get(index);
		if (index >= 0 && index < rss.size()){
			// remove this rule
			rss.remove(index);
			// update active rule list
			listModel.remove(index);
			// notify Stream process 
			//refToStreamapp.deactivateRule(rs.toString());
		}
		// still emit the rule
		// just to make a duplication so that the evaluator can noteice that it is a deactivation
		processRule(rs);
	}
	
	/**
	 * Overwrite this to emit Rule tuple to Stream
	 * @param ruleStr
	 */
	public void processRule(RuleSegment r){
		RuleSegment rs = r;
		collector.emit(new Values(rs));
	}

	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		this.collector = collector;
//		if (collector != null ){
//			System.out.println ("ASSIGNED");
//		}
		// TODO Auto-generated method stub
		SPSMarkPlatController window = new SPSMarkPlatController();
		window.frame.setVisible(true);
		window.collector = collector;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextTuple() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ack(Object msgId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fail(Object msgId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare (fields);
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}
}
