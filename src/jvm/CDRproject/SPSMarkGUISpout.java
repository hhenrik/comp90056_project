package CDRproject;

import java.awt.EventQueue;
import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class SPSMarkGUISpout extends SPSMarkPlatController implements IRichSpout{
	
	private static final long		serialVersionUID	= 1L;
	
	public final static Fields		fields				= new Fields ("cdrRule");
	protected SpoutOutputCollector	collector;
	
	public SPSMarkGUISpout(){
		super();
	}

	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		// TODO Auto-generated method stub
		
		this.collector = collector;
		
		// runs the GUI
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SPSMarkGUISpout window = new SPSMarkGUISpout();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	@Override
	public void processRule(RuleSegment ruleStr){
		collector.emit(new Values(ruleStr));
	}
	
	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextTuple() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ack(Object msgId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fail(Object msgId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
