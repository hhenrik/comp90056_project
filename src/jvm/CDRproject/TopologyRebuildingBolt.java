package CDRproject;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import CDRproject.Utilities.NullPunctuation;
import CDRproject.Utilities.TupleUtilities;
import CDRproject.bolt.BaseWindow.WindowMode;
import CDRproject.bolt.RelayingTumblingWindow;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;


public class TopologyRebuildingBolt implements IRichBolt
{
	private static final long							serialVersionUID	= 1L;

	public static Fields								fields				= TupleRelay.fields;

	protected boolean									debug				= true;
	protected long										rate				= 300;
	protected String									fileName			= "G:\\cdr.rawsample2.txt";

	protected ConcurrentHashMap<RuleSegment, Boolean>	rules;

	protected OutputCollector							collector;

	protected TopologyBuilder							builder;

	protected LocalCluster								cluster;

	protected boolean									hasPrevTopology		= false;


	public TopologyRebuildingBolt ()
	{
		super ();
		this.rules = new ConcurrentHashMap<> ();
	}


	public TopologyRebuildingBolt (ConcurrentHashMap<RuleSegment, Boolean> rules)
	{
		super ();
		this.rules = rules;
	}


	public TopologyRebuildingBolt (ConcurrentHashMap<RuleSegment, Boolean> rules, LocalCluster cluster)
	{
		super ();
		this.rules = rules;
		this.cluster = cluster;
	}


	@ SuppressWarnings ("rawtypes")
	@ Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector)
	{
		this.collector = collector;
	}


	@ Override
	public void execute(Tuple input)
	{
		if (NullPunctuation.isPunct (input))
		{
			NullPunctuation.emitPunctuation (collector, fields.size ());
		}
		else
		{
			if (input.getValue (0) != null) // Rule
			{
				RuleSegment rs = (RuleSegment) input.getValue (0);

				// TODO: ensure it works with containsKey
				// If rule exists, remove it
				if (rules.containsKey (rs))
				{
					rules.remove (rs);
				}
				else
				// if rule doesn't exist, add it
				{
					rules.put (rs, true);
				}

				try
				{
					restartTopology (null);
				}
				catch (Exception e)
				{
					e.printStackTrace ();
					System.exit (0);
				}
			}
			else if (input.getValue (1) != null) // Record
			{
				TupleUtilities.relayTuple (input, collector);
			}
		}
	}


	public void startTopology(String[] args) throws Exception
	{
		// this.cluster = new LocalCluster ();
		rebuildTopology (cluster);
		submitTopology (args);
	}


	public void stopTopology(String[] args) throws Exception
	{
		if (cluster != null)
			cluster.shutdown ();

		// Map conf = Utils.readDefaultConfig ();
		// conf.put (Config.NIMBUS_HOST, "localhost");
		// conf.put (Config.TOPOLOGY_NAME, CDRTopology.TOPOLOGY_NAME);
		// NimbusClient.getConfiguredClient (this.conf).getClient ().killTopology
		// (CDRTopology.TOPOLOGY_NAME);
	}


	public void restartTopology(String[] args) throws Exception
	{
		stopTopology (args);
		Thread.sleep (10 * 1000);

		startTopology (args);
	}


	public void rebuildTopology(LocalCluster clust)
	{
		System.out.println ("Rebuilding topology");
		System.out.println ("\n\n\n\n\n");

		hasPrevTopology = true;

		builder = new TopologyBuilder ();

		// TODO pass rule spout active rules
		builder.setSpout (CDRTopology.CDR_RULE_SPOUT, new SPSMarkPlatController (), 1);
		builder.setSpout (CDRTopology.CDR_VALUE_SPOUT, new CDRWorkloadGen (fileName, rate), 1);

		builder.setBolt (CDRTopology.CDR_RELAY_BOLT, new TupleRelay (), 1).shuffleGrouping (CDRTopology.CDR_VALUE_SPOUT)
				.shuffleGrouping (CDRTopology.CDR_RULE_SPOUT);

		builder.setBolt (CDRTopology.CDR_TOPOLOGY_REBUILD_BOLT, new TopologyRebuildingBolt (this.rules, this.cluster), 1).shuffleGrouping (
				CDRTopology.CDR_RELAY_BOLT);

		int ruleCount = 0;
		final String windowBase = "window-";
		final String aggregatorBase = "agg-";
		final String ruleEvaluatorBase = "rule-eval-";

		int[] timeFrameMultis = {60, 60, 24, 30, 365};
		Enumeration<RuleSegment> topologyLists = rules.keys ();
		while (topologyLists.hasMoreElements ())
		{
			ruleCount++;

			String windowID = windowBase + ruleCount;
			String aggregatorID = aggregatorBase + ruleCount;
			String ruleEvaluatorID = ruleEvaluatorBase + ruleCount;

			RuleSegment rs = topologyLists.nextElement ();

			int timeFrame = 1;
			for (int i = 0; i < rs.TIMEFRAMES.length; i++)
			{
				timeFrame *= timeFrameMultis[i];

				if (rs.TIMEFRAMES[i].equals (rs.getTimeframe ()))
					break;
			}

			timeFrame *= Integer.parseInt (rs.getTimeframe ());

			builder.setBolt (windowID, new RelayingTumblingWindow (WindowMode.TIME, timeFrame, 1, TopologyRebuildingBolt.fields)).shuffleGrouping (
					CDRTopology.CDR_RELAY_BOLT);

			String field = null;
			boolean buildEvaluator = false;
			switch (rs.getTemplate ())
			{
				case 1 :
					// TODO add aggregator here
					// builder.setBolt(aggregatorID, null).shuffleGrouping (windowID);
					// buildEvaluator = true;
					break;
				case 2 :
					// field = CDRRecord.causeForTermination;
					// builder.setBolt (aggregatorID, new CustomTopK (50, field)).shuffleGrouping
					// (windowID);
					// buildEvaluator = true;
					break;
				case 3 :
					// TODO add aggregator here
					// builder.setBolt(aggregatorID, null).shuffleGrouping (windowID);
					// buildEvaluator = true;
					break;
			}

			if (buildEvaluator)
			{
				ArrayList<RuleSegment> usedRules = new ArrayList<> ();
				usedRules.add (rs);
				SPSRuleEvaluator ruleEval = new SPSRuleEvaluator ();
				builder.setBolt (ruleEvaluatorID, ruleEval).shuffleGrouping (aggregatorID);
			}
		}
	}


	public void submitTopology(String[] args) throws Exception
	{
		Config conf = new Config ();
		conf.setDebug (debug);
		conf.setMaxTaskParallelism (3);

		if (cluster == null)
			cluster = new LocalCluster ();
		cluster.submitTopology (CDRTopology.TOPOLOGY_NAME, conf, builder.createTopology ());
	}


	@ Override
	public void cleanup()
	{}


	@ Override
	public void declareOutputFields(OutputFieldsDeclarer declarer)
	{
		declarer.declare (fields);
	}


	@ Override
	public Map<String, Object> getComponentConfiguration()
	{
		return null;
	}
}