package CDRproject;

import java.io.Serializable;
import java.util.Arrays;

/**
 * RuleSegment class stores parameter values for a certain type of rule template
 * provides interface for both the Stream app and GUI Controller
 * 
 * @author Henrik
 *
 */
public class RuleSegment implements Serializable{

	public static final String[] TEMPLATES  = {"TEMPLATE_1", "TEMPLATE_2", "TEMPLATE_3"};
	public static final String[] CALLTYPES  = {"INTERNATIONAL", "LOCAL"};
	public static final String[] OPERATIONS = {">", "<", ">=", "<=", "==", "!="};
	public static final String[] TIMEFRAMES = {"MINS", "HOURS", "DAYS", "MONTHS", "YEARS"};
	public static final String[] PROMOTIONS = {"PROMO_1", "PROMO_2", "PROMO_3", "PROMO_4"};
	
	public static final int[] timeFrameMultis = {60, 60, 24, 30, 365};
	
	private int template;
	private String timeFrame;
	private String promo;
	private int value1;
	private int value2;
	private String calltype;
	
	public RuleSegment(){
		template = 0;
		timeFrame = null;
		value1 = 0;
		value2 = 0;
		promo = null;
		calltype = null;
	}
	
	public RuleSegment(String str){
		template = 0;
		timeFrame = null;
		value1 = 0;
		value2 = 0;
		promo = null;
		calltype = null;
		setFromString(str);
	}
	
	public static int timeFrameToSeconds(int tfIndex, int val)
	{
		int timeFrame = val;
		for (int i = 0; i <= tfIndex; i++)
		{
			timeFrame *= timeFrameMultis[i];
		}
		
		return timeFrame;
	}
	
	/*
	 * Settors
	 */
	public void setTemplate(int temp){
		this.template = temp;
	}
	
	public void setOP(String op){
		// 
	}
	
	public void setTimeframe(String timeFrame){
		
		this.timeFrame = timeFrame;
	}
	
	/* should check if it can be parsed into a number */
	public void setValue1(int value1){
		this.value1 = value1;
	}
	
	/* should check if it can be parsed into a number */
	public void setValue2(int value2){
		this.value2 = value2;
	}
	
	public void setPromo(String promo){
		this.promo = promo;
	}
	
	public void setCallType(String ct){
		this.calltype = ct;
	}
	/*
	 * Gettors
	 */
	public int getTemplate(){
		return this.template;
	}
	
	public String getOP(){
		// 
		return null;
	}
	
	public String getTimeframe(){
		return this.timeFrame;
	}
	
	public int getValue1(){
		return this.value1;
	}
	
	public int getValue2(){
		return this.value2;
	}
	
	public String getPromo(){
		return this.promo;
	}
	
	public String getCallType(){
		return this.calltype;
	}
	
	/*
	 * Utility functions
	 */
	/**
	 * toString
	 */
	public String printRS(){
	
		switch(this.template){
		case 1: // template 1 rule segment
			return "" + this.template+ "," + this.value1 + "," + this.calltype +"," + this.value2 + "," + this.timeFrame + "," + this.promo ;
		case 2: // template 2 rule segment
			return "" + this.template+ "," + this.value1 +"," + this.value2 + "," + this.timeFrame + "," + this.promo ;
		case 3: // template 3 rule segment
			return "" + this.template+ "," + this.value2 + "," + this.timeFrame + "," + this.promo ;
		default:
			return "";
		}
		
	}
	
	/**
	 * Reconstruct a Rule Segment from a String
	 * can only be called if the input String was a Rule Segment converted by toString method
	 * @param str
	 * @return
	 */
	public void setFromString(String str){
		String[] segs = str.split(",");
		setTemplate(Integer.parseInt(segs[0]));
		switch(this.template){
		case 1: // template 1 rule segment
			setValue1(Integer.parseInt(segs[1]));
			setCallType(segs[2]);
			setValue2(Integer.parseInt(segs[3]));
			setTimeframe(segs[4]);
			setPromo(segs[5]);
			break;
		case 2: // template 2 rule segment
			setValue1(Integer.parseInt(segs[1]));
			setValue2(Integer.parseInt(segs[2]));
			setTimeframe(segs[3]);
			setPromo(segs[4]);
		case 3: //template 3 rule segment
			setValue2(Integer.parseInt(segs[1]));
			setTimeframe(segs[2]);
			setPromo(segs[3]);
		default:
			break;
		}
		System.out.println(segs.length);
	}
	
	/**
	 * equals, check if it is same with the other rule segment
	 * @param rs
	 * @return
	 */
	public boolean equals(Object o){
		RuleSegment rs = (RuleSegment)o;
		switch(this.template){
		case 1: // template 1 rule segment
			return (rs.getTemplate()==this.template) && rs.getTimeframe().equals(this.timeFrame) && rs.getPromo().equals(this.promo)
					&& (rs.getValue1()==this.value1) && (rs.getValue2()==this.value2) && rs.getCallType().equals(this.calltype);
					
		case 2: // template 2 rule segment
			return (rs.getTemplate()==this.template) && rs.getTimeframe().equals(this.timeFrame) && rs.getPromo().equals(this.promo)
					&& (rs.getValue1()==this.value1) && (rs.getValue2()==this.value2) ;
		case 3: 
			return (rs.getTemplate()==this.template) && rs.getTimeframe().equals(this.timeFrame) && rs.getPromo().equals(this.promo)
					&& (rs.getValue2()==this.value2);
		default:
			return false;
		}
	}
	
	/**
	 * Test
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RuleSegment rs1 = new RuleSegment();
		String str = "1,3,int,2,DAYS,PROMO_3";
		rs1.setFromString(str);
		System.out.println(rs1.toString());
		RuleSegment rs2 = new RuleSegment();
		String str2 = "2,3,2,DAYS,PROMO_3";
		rs2.setFromString(str2);
		System.out.println(rs2.toString());
	}

}
