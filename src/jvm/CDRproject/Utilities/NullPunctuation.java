package CDRproject.Utilities;


import java.util.Arrays;


import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.OutputCollector;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


/**
 * Basic null punctuation type. Assumes punctuation is an array filled only with null values
 * 
 * @author Aniruddh Fichadia (Ani.Fichadia@gmail.com)
 */
public class NullPunctuation
{
	// =============================== Methods ===============================
	public static void emitPunctuation(BasicOutputCollector collector, int numFields)
	{
		collector.emit (new Values (getPunct (numFields)));
	}


	public static void emitPunctuation(SpoutOutputCollector collector, int numFields)
	{
		collector.emit (new Values (getPunct (numFields)));
	}


	public static void emitPunctuation(OutputCollector collector, int numFields)
	{
		collector.emit (new Values (getPunct (numFields)));
	}


	public static boolean isPunct(Tuple input)
	{
		int size = input.size ();
		for (int i = 0; i < size; i++)
		{
			if (input.getValue (i) != null)
				return false;
		}

		return true;
	}


	public static Object[] getPunct(int numFields)
	{
		Object[] fields = new Object[numFields];
		Arrays.fill (fields, null);
		return fields;
	}
}