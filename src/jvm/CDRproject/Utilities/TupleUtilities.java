package CDRproject.Utilities;


import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


public class TupleUtilities
{
	// =============================== Methods ===============================
	public static void relayTuple(Tuple input, OutputCollector collector)
	{
		collector.emit (tupleToValues (input));
	}


	public static Values tupleToValues(Tuple tuple)
	{
		Values vals = new Values ();
		for (int v = 0; v < tuple.size (); v++)
		{
			vals.add (tuple.getValue (v));
		}

		return vals;
	}
}