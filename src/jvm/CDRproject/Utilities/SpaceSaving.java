package CDRproject.Utilities;


import java.io.Serializable;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;


public class SpaceSaving<E> implements Serializable
{
	// ============================= Attributes ==============================
	private static final long				serialVersionUID	= 1L;

	protected final int						K;

	protected ConcurrentHashMap<E, Integer>	frequencies;


	// ============================ Constructors =============================
	public SpaceSaving (int k)
	{
		super ();
		this.K = k;
		this.frequencies = new ConcurrentHashMap<> ();
	}


	// =============================== Methods ===============================
	public boolean processElem(E elem)
	{
		if (elem == null)
			return false;

		if (frequencies.containsKey (elem))
		{
			int updatedFreq = frequencies.get (elem) + 1;
			frequencies.put (elem, updatedFreq);
		}
		else if (frequencies.size () < K)
			frequencies.put (elem, 1);
		else
		{
			E smallestKey = smallestKey ();
			int smallestVal = frequencies.get (smallestKey);

			frequencies.put (elem, smallestVal + 1);
			frequencies.remove (smallestKey);
		}

		return true;
	}


	protected E smallestKey()
	{
		Enumeration<E> keys = frequencies.keys ();
		E smallestKey = null;
		int smallestVal = 0;

		while (keys.hasMoreElements ())
		{
			if (smallestKey == null)
			{
				smallestKey = keys.nextElement ();
				smallestVal = frequencies.get (smallestKey);
			}
			else
			{
				E key = keys.nextElement ();
				int val = frequencies.get (key);

				if (val < smallestVal)
				{
					smallestKey = key;
					smallestVal = val;
				}
			}
		}
		return smallestKey;
	}


	public void clear()
	{
		frequencies.clear ();
	}


	// ========================== Getters & Setters ==========================
	public ConcurrentHashMap<E, Integer> getFrequencies()
	{
		return frequencies;
	}


	public int getK()
	{
		return K;
	}
}