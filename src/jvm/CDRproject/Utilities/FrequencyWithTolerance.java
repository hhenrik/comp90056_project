package CDRproject.Utilities;


import java.io.Serializable;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;


public class FrequencyWithTolerance<K> implements Serializable
{
	// ============================= Attributes ==============================
	private static final long				serialVersionUID	= 1L;

	protected final int						K;
	protected final int						TOLERANCE;

	protected ConcurrentHashMap<K, Integer>	frequencies;


	// ============================ Constructors =============================
	public FrequencyWithTolerance (int k, int tolerance)
	{
		super ();
		this.K = k;
		this.TOLERANCE = tolerance;
		this.frequencies = new ConcurrentHashMap<> ();
	}


	// =============================== Methods ===============================
	public void processElem(K elem)
	{
		if (elem == null)
			return;

		if (frequencies.containsKey (elem))
			frequencies.put (elem, frequencies.get (elem) + 1);
		else if (frequencies.size () < (K + TOLERANCE))
			frequencies.put (elem, 1);
		else
		{
			Enumeration<K> keys = frequencies.keys ();
			while (keys.hasMoreElements ())
			{
				K key = keys.nextElement ();
				int newVal = frequencies.get (key) - 1;

				if (newVal > 0)
					frequencies.put (key, newVal);
				else
					frequencies.remove (key);
			}
		}
	}


	public void clear()
	{
		this.frequencies.clear ();
	}


	public ConcurrentHashMap<K, Integer> getTopK()
	{
		ConcurrentHashMap<K, Integer> temp = new ConcurrentHashMap<> (frequencies);

		ConcurrentHashMap<K, Integer> topK = new ConcurrentHashMap<> ();

		while (topK.size () < K && temp.size () > 0)
		{
			Enumeration<K> keys = temp.keys ();

			K largestKey = keys.nextElement ();
			int largestVal = temp.get (largestKey);

			while (keys.hasMoreElements ())
			{
				K key = keys.nextElement ();

				if (temp.get (key) > largestVal)
				{
					largestKey = key;
					largestVal = temp.get (key);
				}
			}

			if (largestKey != null)
			{
				topK.put (largestKey, temp.get (largestKey));
				temp.remove (largestKey);
			}
		}

		return topK;
	}


	// ========================== Getters & Setters ==========================
	public int getK()
	{
		return K;
	}


	public ConcurrentHashMap<K, Integer> getFrequencies()
	{
		return frequencies;
	}


	public void setFrequencies(ConcurrentHashMap<K, Integer> frequencies)
	{
		this.frequencies = frequencies;
	}


	// ================================ String ===============================
	@ Override
	public String toString()
	{
		return asString ();
	}


	public String asString()
	{
		StringBuilder sb = new StringBuilder ();

		Enumeration<K> keys = frequencies.keys ();

		while (keys.hasMoreElements ())
		{
			K key = keys.nextElement ();

			sb.append (key.toString ());
			sb.append ("\t");
			sb.append (frequencies.get (key));
			sb.append ("\n");
		}

		return sb.toString ();
	}
}