package CDRproject.Utilities;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * processes two lists of tweets and computes the average similarity between them
 * 
 * @author Henrik
 * 
 * @param <T>
 */
public class Similarity
{
	public static final String	splitRegex	= "[\\p{Punct}+\\s+]";
	// list of tweets from two different users
	ArrayList<String>			tweets_1;
	ArrayList<String>			tweets_2;

	String						t1;
	String						t2;
	// word frequency table for each of tweet
	Hashtable<String, Integer>	wFreq1;
	Hashtable<String, Integer>	wFreq2;
	double						score;


	// constructor
	public Similarity (ArrayList<String> tweets1, ArrayList<String> tweets2)
	{
		this.tweets_1 = tweets1;
		this.tweets_2 = tweets2;
		score = 0.0;
	}


	// computes the average similarity score between two users over their total tweets
	public double func()
	{
		double sumscore = 0.0;
		double n = 0;
		for (int i = 0; i < tweets_1.size (); i++)
		{
			for (int j = 0; j < tweets_2.size (); j++)
			{

				initializeTables (tweets_1.get (i), tweets_2.get (j));
				sumscore += computeMetric ();
				n++;
			}
		}
		return sumscore / n;
	}


	// for each pair of tweets, computes their similarity
	public double computeMetric()
	{
		double g, ff1, ff2;
		g = 0.0;
		ff1 = 0.0;
		ff2 = 0.0;
		Enumeration<String> keys = wFreq1.keys ();
		// compute g
		while (keys.hasMoreElements ())
		{
			String key = keys.nextElement ();
			// System.out.println(key);
			if (wFreq2.containsKey (key))
			{
				// System.out.println("contains" + wFreq1.get(key) + " " + wFreq2.get(key));

				g += (((double) wFreq1.get (key)) / t1.length ()) * (((double) wFreq2.get (key)) / t2.length ());

				// }else{
				// // debug
				// System.out.println("not");
			}

		}
		// System.out.println("g " + g);
		// compute ff1, the sum of the squares of word frequency f1
		keys = wFreq1.keys ();
		while (keys.hasMoreElements ())
		{
			String key = keys.nextElement ();
			ff1 += Math.pow ((double) wFreq1.get (key) / t1.length (), 2);
		}
		ff1 = Math.sqrt (ff1);

		// compute ff2, the sum of the squares of word frequency f2
		keys = wFreq2.keys ();
		while (keys.hasMoreElements ())
		{
			String key = keys.nextElement ();
			ff2 += Math.pow ((double) wFreq2.get (key) / t2.length (), 2);
		}
		ff2 = Math.sqrt (ff2);

		return g / (ff1 * ff2);
	}


	public void setT1T2(String t1, String t2)
	{
		this.t1 = t1;
		this.t2 = t2;
	}


	public void initializeTables(String tt1, String tt2)
	{
		setT1T2 (tt1, tt2);
		wFreq1 = new Hashtable<String, Integer> ();
		wFreq2 = new Hashtable<String, Integer> ();
		String[] words1 = splitText (t1);
		String[] words2 = splitText (t2);
		for (int i = 0; i < words1.length; i++)
		{
			words1[i] = sanitizeWord (words1[i]);
		}
		for (int i = 0; i < words2.length; i++)
		{
			words2[i] = sanitizeWord (words2[i]);
		}

		for (String s : words1)
		{
			if (wFreq1.containsKey (s))
			{
				int f = wFreq1.get (s);
				wFreq1.put (s, f + 1);
			}
			else
			{
				wFreq1.put (s, 1);
			}
		}

		for (String s : words2)
		{
			if (wFreq2.containsKey (s))
			{
				int f = wFreq2.get (s);
				wFreq2.put (s, f + 1);
			}
			else
			{
				wFreq2.put (s, 1);
			}
		}
	}


	public String[] splitText(String sentence)
	{
		return sentence.split (splitRegex);
	}


	public String sanitizeWord(String word)
	{
		return word.trim ().toLowerCase ();
	}
}