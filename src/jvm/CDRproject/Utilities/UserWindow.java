package CDRproject.Utilities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 
 * @author Henrik
 * 
 * @param <E>
 */
public class UserWindow<K, T> implements Serializable
{

	// ============================= Attributes ==============================
	private static final long						serialVersionUID	= 1L;

	protected ConcurrentHashMap<K, ArrayList<T>>	users;


	// ============================ Constructors =============================
	public UserWindow ()
	{
		super ();
		this.users = new ConcurrentHashMap<K, ArrayList<T>> ();
	}


	// =============================== Methods ===============================
	// get the list of tweets that a user has tweeted
	public ArrayList<T> getTweet(K k)
	{
		ArrayList<T> temp = null;
		if (users.containsKey (k))
		{
			temp = users.get (k);
		}
		return temp;
	}


	// add a new tweet t to a specific user k
	public void addTweet(K k, T t)
	{
		ArrayList<T> temp = getTweet (k);
		if (temp != null)
		{
			temp.add (t);
			users.put (k, temp);
		}
		else
		{
			temp = new ArrayList<T> ();
			temp.add (t);
			users.put (k, temp);
		}
	}


	// ========================== Getters & Setters ==========================
	public ConcurrentHashMap<K, ArrayList<T>> getUsers()
	{
		return users;
	}


	public void clear()
	{
		users.clear ();
	}
}