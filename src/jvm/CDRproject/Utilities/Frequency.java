package CDRproject.Utilities;


import java.io.Serializable;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;


public class Frequency<K> implements Serializable
{
	// ============================= Attributes ==============================
	private static final long				serialVersionUID	= 1L;

	protected final int						K;

	protected ConcurrentHashMap<K, Integer>	frequencies;


	// ============================ Constructors =============================
	public Frequency (int k)
	{
		super ();
		this.K = k;
		this.frequencies = new ConcurrentHashMap<> (K);
	}


	// =============================== Methods ===============================
	public void processElem(K elem)
	{
		if (elem == null)
			return;

		if (frequencies.containsKey (elem))
			frequencies.put (elem, frequencies.get (elem) + 1);
		else if (frequencies.size () < K)
			frequencies.put (elem, 1);
		else
		{
			Enumeration<K> keys = frequencies.keys ();
			while (keys.hasMoreElements ())
			{
				K key = keys.nextElement ();
				int newVal = frequencies.get (key) - 1;

				if (newVal > 0)
					frequencies.put (key, newVal);
				else
					frequencies.remove (key);
			}
		}
	}


	public void clear()
	{
		this.frequencies.clear ();
	}


	// ========================== Getters & Setters ==========================
	public int getK()
	{
		return K;
	}


	public ConcurrentHashMap<K, Integer> getFrequencies()
	{
		return frequencies;
	}


	public void setFrequencies(ConcurrentHashMap<K, Integer> frequencies)
	{
		this.frequencies = frequencies;
	}


	// ================================ String ===============================
	@ Override
	public String toString()
	{
		return asString ();
	}


	public String asString()
	{
		StringBuilder sb = new StringBuilder ();

		Enumeration<K> keys = frequencies.keys ();

		while (keys.hasMoreElements ())
		{
			K key = keys.nextElement ();

			sb.append (key.toString ());
			sb.append ("\t");
			sb.append (frequencies.get (key));
			sb.append ("\n");
		}

		return sb.toString ();
	}
}