package CDRproject;


import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;


/**
 * CDRRecord class
 * 
 * @author Henrik
 */
public class CDRRecord implements Serializable
{
	private static final long					serialVersionUID	= 1L;
	/*
	 * Attributes
	 */
	public static final String					MTC					= "MTC";
	public static final String					MOT					= "MOC";
	public static final String					cdrType				= "CDRTYPE";
	public static final String					imsi				= "IMSI";
	public static final String					imei				= "IMEI";
	public static final String					callingNumber		= "CALLINGNUMBER";
	public static final String					calledNumber		= "CALLEDNUMBER";
	public static final String					recordingEntity		= "RECORDINGENTITY";
	public static final String					location			= "LOCATION";
	public static final String					callReference		= "CALLREFERENCE";
	public static final String					callDuration		= "CALLDURATION";
	public static final String					answerTime			= "ANSWERTIME";
	public static final String					seizureTime			= "SEIZURETIME";
	public static final String					releaseTime			= "RELEASETIME";
	public static final String					causeForTermination	= "CAUSEFORTERMINATION";
	public static final String					basicService		= "BASICSERVICE";
	public static final String					mscAddress			= "MSCADDRESS";

	final public String[]						CDRFIELDS			= {
			cdrType,
			imsi,
			imei,
			callingNumber,
			calledNumber,
			recordingEntity,
			location,
			callReference,
			callDuration,
			answerTime,
			seizureTime,
			releaseTime,
			causeForTermination,
			basicService,
			mscAddress												};
	private ConcurrentHashMap<String, String>	record;
	
	public CDRRecord (String str)
	{
		record = new ConcurrentHashMap<String, String> ();
		String[] attr = str.split (",");

		if (attr.length == CDRFIELDS.length)
		{
			for (int i = 0; i < CDRFIELDS.length; i++)
			{
				record.put (CDRFIELDS[i], attr[i].replaceAll ("\"", ""));
			}
		}
		else
		{
			System.out.println ("CDRRecord fields not match");
		}
	}


	public void printRecord()
	{
		for (String i : CDRFIELDS)
		{
			System.out.println (i + " " + record.get (i));
		}
	}


	/*
	 * Gettors
	 * - create others if needed
	 */
	public String getCallingNumber()
	{
		return record.get (callingNumber);
	}

	public String getCalledNumber(){
		return record.get(calledNumber);
	}
	
	public String getCDRType(){
		return record.get(cdrType);
	}
	
	public String getCauseForTermination(){
		return record.get(causeForTermination);
	}
	
	public String getLocation(){
		return record.get(location);
	}

	/**
	 * Test
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		String str = "\"MOC\",\"301007645565635\",\"017881044129250\",\"18016754110\",\"12282120300\",\"000006623839681\",12879,1228,2171.2,\"2013-09-10 07:46:44\",\"\",\"2013-09-10 08:22:55\",\"00\",\"11\",\"000006623839681\"";
		CDRRecord newRecord = new CDRRecord (str);
		newRecord.printRecord ();
	}


	public ConcurrentHashMap<String, String> getRecord()
	{
		return record;
	}
}
