package CDRproject;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;


import CDRproject.Utilities.NullPunctuation;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;


/**
 * CDRWorkloadGen reads data file and simulates as a data stream with different rates.
 * - This is needed for testing and performance analysis of application
 * 
 * @author Henrik
 */
public class CDRWorkloadGen implements IRichSpout
{
	private static final long		serialVersionUID	= 1L;

	public final static Fields		fields				= new Fields ("cdrRecord");

	protected SpoutOutputCollector	collector;

	/** Encoding of file. Assumes it is StandardCharsets.ISO_8859_1 */
	protected final static Charset	ENCODING			= StandardCharsets.ISO_8859_1;

	/** the number of records emit per second */
	private double					rate;
	private BufferedReader			reader;
	protected String				fileName;

	protected boolean				hasRun				= false;


	public CDRWorkloadGen (String fileName, double rate)
	{
		this.rate = rate;
		this.fileName = fileName;
	}


	public static void main(String[] args)
	{
		CDRWorkloadGen test = new CDRWorkloadGen ("C:\\Users\\Henrik\\Downloads\\cdr_rawsample1.txt", 300);
	}


	/**
	 * Overwrite this method for processing line
	 * - Print line for testing
	 * 
	 * @param line
	 */
	public void processLine(String line)
	{
		this.collector.emit (new Values (new CDRRecord (line)));
//		System.out.println (line);
	}


	@ SuppressWarnings ("rawtypes")
	@ Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector)
	{
		this.collector = collector;
	}


	@ Override
	public void nextTuple()
	{
		if ( !hasRun)
		{
			Path path = Paths.get (this.fileName);

			try
			{
				reader = Files.newBufferedReader (path, ENCODING);
				String line;
				while ((line = reader.readLine ()) != null)
				{
					processLine (line);
					if (rate > 0)
						Thread.sleep ((long) (1000 / rate));
					// System.out.println((long)(1000/rate));
				}
				reader.close ();
			}
			catch (IOException | InterruptedException e)
			{
				e.printStackTrace ();
			}

			NullPunctuation.emitPunctuation (collector, fields.size ());
			hasRun = true;
		}
	}


	@ Override
	public void close()
	{}


	@ Override
	public void activate()
	{}


	@ Override
	public void deactivate()
	{}


	@ Override
	public void ack(Object msgId)
	{}


	@ Override
	public void fail(Object msgId)
	{}


	@ Override
	public void declareOutputFields(OutputFieldsDeclarer declarer)
	{
		declarer.declare (fields);
	}


	@ Override
	public Map<String, Object> getComponentConfiguration()
	{
		return null;
	}
}