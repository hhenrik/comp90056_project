package CDRproject;


import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;


import CDRproject.bolt.BaseWindow.WindowMode;
import CDRproject.bolt.RelayingTumblingWindow;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;


public class CDRTopology
{
	public static final String	TOPOLOGY_NAME				= "cdr-processor";

	public static final String	CDR_VALUE_SPOUT				= "cdr-value-spout";

	public static final String	CDR_RULE_SPOUT				= "cdr-rule-spout";

	public static final String	CDR_RELAY_BOLT				= "cdr-relay-bolt";

	public static final String	CDR_TOPOLOGY_REBUILD_BOLT	= "cdr-topology-rebuild-bolt";

	public static final String	CDR_RULE_EVALUATOR			= "cdr-rule-evaluator";

	public static final String	CDR_RULE_1_WINDOW			= "cdr-rule-1-window";
	public static final String	CDR_RULE_1_AGG				= "cdr-rule-1-agg";

	public static final String	CDR_RULE_2_WINDOW			= "cdr-rule-2-window";
	public static final String	CDR_RULE_2_AGG				= "cdr-rule-2-agg";

	public static final String	CDR_RULE_3_WINDOW			= "cdr-rule-3-window";
	public static final String	CDR_RULE_3_AGG				= "cdr-rule-3-agg";


	public static void main(String[] args) throws Exception
	{
		boolean debug = false;
		double rate = 10000; // the number of tuples to be emitted each sec
		// TODO reset default
		String fileName = "G:\\cdr.rawsample2.txt";

		TopologyBuilder builder = new TopologyBuilder ();
		// SPOUTS
		builder.setSpout (CDR_VALUE_SPOUT, new CDRWorkloadGen (fileName, rate));
		builder.setSpout (CDR_RULE_SPOUT, new SPSMarkPlatController ());

		// BOLTS
		builder.setBolt (CDR_RELAY_BOLT, new TupleRelay ()).shuffleGrouping (CDR_VALUE_SPOUT).shuffleGrouping (CDR_RULE_SPOUT);

		builder.setBolt (CDR_RULE_EVALUATOR, new SPSRuleEvaluator ()).shuffleGrouping (CDR_RELAY_BOLT);

		int windowTF = 1 * 60;
		builder.setBolt (CDR_RULE_1_WINDOW, new RelayingTumblingWindow (WindowMode.TIME, windowTF, 1, SPSRuleEvaluator.fields)).shuffleGrouping (
				CDR_RULE_EVALUATOR);
		builder.setBolt (CDR_RULE_1_AGG, new CustomTopK (100, CDRRecord.callingNumber, 1)).shuffleGrouping (CDR_RULE_1_WINDOW);

		builder.setBolt (CDR_RULE_2_WINDOW, new RelayingTumblingWindow (WindowMode.TIME, windowTF, 2, SPSRuleEvaluator.fields)).shuffleGrouping (
				CDR_RULE_EVALUATOR);
		builder.setBolt (CDR_RULE_2_AGG, new CustomTopK (100, CDRRecord.callingNumber, 2)).shuffleGrouping (CDR_RULE_2_WINDOW);

		builder.setBolt (CDR_RULE_3_WINDOW, new RelayingTumblingWindow (WindowMode.TIME, windowTF, 3, SPSRuleEvaluator.fields)).shuffleGrouping (
				CDR_RULE_EVALUATOR);
		builder.setBolt (CDR_RULE_3_AGG, new CustomTopK (10, CDRRecord.callingNumber, 3)).shuffleGrouping (CDR_RULE_3_WINDOW);

		Config conf = new Config ();
		conf.setDebug (debug);

		if (args != null && args.length > 0)
		{
			System.out.println ("Using storm submitter");
			conf.setNumWorkers (3);
			StormSubmitter.submitTopology (args[0], conf, builder.createTopology ());
		}
		else
		{
			System.out.println ("Using Local Cluster");

			conf.setMaxTaskParallelism (3);

			LocalCluster cluster = new LocalCluster ();
			cluster.submitTopology (TOPOLOGY_NAME, conf, builder.createTopology ());

			// Run cluster for 1 hour,
			Thread.sleep (1000 * 60 * 60);

			// If uncommented, prevents cluster from shutting down until input is provided into the
			// console
			// int x = System.in.read ();

			// Then shutdown cluster
			cluster.shutdown ();
		}
	}
}