package CDRproject;

import java.util.Observable;

/**
 * Abstract Class provides interface to access the Stream application
 * @author Henrik
 *
 */
public abstract class SPSMarkPlatTemp extends Observable{

	/**
	 * apply Rule specified by GUI Controller to the Stream process
	 * @param str
	 */
	public abstract void activateRule(String str);
	
	/**
	 * remove Rule specified by GUI Controller from the Stream process
	 * @param str
	 */
	public abstract void deactivateRule(String str);
	
	/**
	 * Get the list of users who are qualified for promotions
	 * @return
	 */
	public abstract Object getPromotedUsers();
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
