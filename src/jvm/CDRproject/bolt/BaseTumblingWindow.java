package CDRproject.bolt;


import java.util.ArrayList;
import java.util.List;


import storm.starter.util.TupleHelpers;
import CDRproject.Utilities.NullPunctuation;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;


public class BaseTumblingWindow extends BaseWindow
{
	// ============================= Attributes ==============================
	private static final long	serialVersionUID	= 1L;

	protected List<Tuple>		windowContents;


	// ============================ Constructors =============================
	public BaseTumblingWindow (WindowMode windowMode, int windowConstraint, Fields fields)
	{
		super (windowMode, windowConstraint, fields);

		switch (this.windowMode)
		{
			case SIZE :
				windowContents = new ArrayList<> (windowConstraint);
				break;
			case TIME :
				windowContents = new ArrayList<> ();
				break;
		}
	}


	// ============================== Inherited ==============================
	@ Override
	public void execute(Tuple input)
	{
		if (NullPunctuation.isPunct (input))
		{
			flushWindow ();
			NullPunctuation.emitPunctuation (collector, fields.size ());
		}
		else
		{
			switch (windowMode)
			{
				case SIZE :
					windowContents.add (input);

					if (windowContents.size () >= windowConstraint)
					{
						flushWindow ();
					}
					break;
				case TIME :
					if (TupleHelpers.isTickTuple (input))
						flushWindow ();
					else
						windowContents.add (input);
					break;
			}
		}
	}


	@ Override
	protected void flushWindow()
	{
		// Emit window contents
		emitList (windowContents);

		clear ();
		NullPunctuation.emitPunctuation (collector, fields.size ());
	}


	@ Override
	protected void clear()
	{
		// Clear window
		windowContents.clear ();
	}
}