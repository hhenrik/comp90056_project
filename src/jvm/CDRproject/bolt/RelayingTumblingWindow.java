package CDRproject.bolt;


import java.util.ArrayList;


import storm.starter.util.TupleHelpers;
import CDRproject.Utilities.NullPunctuation;
import CDRproject.Utilities.TupleUtilities;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;


/**
 * Doesn't store tuples. instead, relays tuples and once the window flushCondition is met, sends a
 * null
 * punctuation. This is more space efficient for large periods of time, since any aggregation, etc
 * that occurs with bolts receiving tuples from the window manage their flushes based on
 * NullPunctuations
 */
public class RelayingTumblingWindow extends BaseTumblingWindow
{
	private static final long	serialVersionUID	= 1L;

	int							tupleCount			= 0;

	private int					templateID;


	public RelayingTumblingWindow (WindowMode windowMode, int windowConstraint, int templateid, Fields fields)
	{
		super (windowMode, windowConstraint, fields);
		templateID = templateid;
		// No need to use windowContents. Set to null since it's unused.
		this.windowContents = null;
	}


	@ Override
	public void execute(Tuple input)
	{
		// System.out.println("<====== WINDOWING ======>");
		if (NullPunctuation.isPunct (input))
		{
			// If you receive a punct, forward it
			NullPunctuation.emitPunctuation (collector, fields.size ());
		}
		else
		{
			// check if if the tuple matches the template first
			try
			{
				@ SuppressWarnings ("unchecked")
				ArrayList<Integer> templateIDs = (ArrayList<Integer>) input.getValue (2);
				if (templateIDs != null && templateIDs.contains (templateID))
				{
					// If you receive a normal tuple, just forward it. Do not store since it will
					// consume a
					// significant amount of space for long time periods
					// Evaluate flushCondition.
					switch (windowMode)
					{
						case SIZE :
							// Base size WindowMode on tupleCount.
							tupleCount++;
							TupleUtilities.relayTuple (input, collector);

							if (tupleCount >= windowConstraint)
							{
								// reset tuple count
								tupleCount = 0;
								// Emit punctuation
								NullPunctuation.emitPunctuation (collector, fields.size ());
							}
							break;
						case TIME :
							if (TupleHelpers.isTickTuple (input)) // Send punctuation
							{
								NullPunctuation.emitPunctuation (collector, fields.size ());
							}
							else
							{
								// Relay tuple
								TupleUtilities.relayTuple (input, collector);
								tupleCount++;
							}
							break;
					}
				}
			}
			catch (Exception e)
			{}
			
			switch (windowMode)
			{
				case SIZE :
					break;
				case TIME :
					if (TupleHelpers.isTickTuple (input)) // Send punctuation
					{
						NullPunctuation.emitPunctuation (collector, fields.size ());
						System.out.println(this.templateID + ", tuples processed per minute: " + tupleCount);
						tupleCount = 0;
					}
					break;
			}

		}
	}
}
