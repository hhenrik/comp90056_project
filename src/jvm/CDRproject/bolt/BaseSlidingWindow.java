package CDRproject.bolt;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import storm.starter.util.TupleHelpers;
import CDRproject.Utilities.NullPunctuation;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;


public class BaseSlidingWindow extends BaseWindow
{
	// ============================= Attributes ==============================
	private static final long			serialVersionUID	= 1L;

	protected final int					windowSize;

	protected LinkedList<Tuple>			windowContentsSize;
	protected LinkedList<List<Tuple>>	windowContentsTime;


	// ============================ Constructors =============================
	/**
	 * @param windowMode Window mode
	 * @param windowConstraint Flush constraint
	 * @param windowSize Window size, must be a multiple of windowConstraint
	 * @param fields Fields for stored tuples
	 */
	public BaseSlidingWindow (WindowMode windowMode, int windowConstraint, int windowSize, Fields fields)
	{
		super (windowMode, windowConstraint, fields);

		if (windowSize % windowConstraint != 0)
		{
			// TODO throw error?
			// throw new IllegalArgumentException
			// ("windowConstraint must be a factor of windowSize");
		}

		this.windowSize = windowSize;
		switch (windowMode)
		{
			case SIZE :
				this.windowContentsSize = new LinkedList<Tuple> ();
				break;
			case TIME :
				this.windowContentsTime = new LinkedList<List<Tuple>> ();
				break;
		}
	}


	@ Override
	public void execute(Tuple input)
	{
		if (NullPunctuation.isPunct (input))
		{
			flushWindow ();
		}
		else
		{
			switch (windowMode)
			{
				case SIZE :
					// Add to window
					windowContentsSize.add (input);

					// If size is greater than window size, remove first.
					if (windowContentsSize.size () > windowSize)
					{
						windowContentsSize.removeFirst ();
					}

					// If size is a flush trigger, flush.
					if (windowContentsSize.size () % windowConstraint == 0)
					{
						flushWindow ();
					}
					break;
				case TIME :
					if (TupleHelpers.isTickTuple (input))
					{
						flushWindow ();

						int numBuckets = windowSize / windowConstraint;
						if (windowContentsTime.size () >= numBuckets)
						{
							// remove first
							windowContentsTime.removeFirst ();
						}

						// add new last
						windowContentsTime.addLast (new ArrayList<Tuple> ());
					}
					else
					{
						// Ensure one bucket exists
						if (windowContentsTime.size () == 0)
						{
							windowContentsTime.add (new ArrayList<Tuple> ());
						}

						// Get last and add to last
						windowContentsTime.getLast ().add (input);
					}
					break;
			}
		}
	}


	@ Override
	protected void flushWindow()
	{
		switch (windowMode)
		{
			case SIZE :
				// Emit window contents
				emitList (windowContentsSize);
				break;
			case TIME :
				for (List<Tuple> list : windowContentsTime)
				{
					emitList (list);
				}
				break;
		}

		NullPunctuation.emitPunctuation (collector, fields.size ());
	}


	@ Override
	protected void clear()
	{
		switch (windowMode)
		{
			case SIZE :
				windowContentsSize.clear ();
				break;
			case TIME :
				windowContentsTime.clear ();
				break;
		}
	}
}