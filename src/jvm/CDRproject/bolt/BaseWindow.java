package CDRproject.bolt;


import java.util.List;
import java.util.Map;




import CDRproject.Utilities.TupleUtilities;
import backtype.storm.Config;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;


public abstract class BaseWindow extends BaseRichBolt
{
	// ============================= Attributes ==============================
	private static final long	serialVersionUID	= 1L;

	protected final Fields		fields;


	public enum WindowMode
	{
		TIME, SIZE;
	}


	/** Window mode */
	protected final WindowMode	windowMode;

	/** A time (seconds) or size value, which triggers the window to flush */
	protected final int			windowConstraint;

	protected OutputCollector	collector;


	// ============================ Constructors =============================
	public BaseWindow (WindowMode windowMode, int windowConstraint, Fields fields)
	{
		super ();
		this.windowMode = windowMode;
		this.windowConstraint = windowConstraint;
		this.fields = fields;
	}


	// =============================== Methods ===============================
	protected void emitList(List<Tuple> list)
	{
		for (Tuple tuple : list)
		{
			TupleUtilities.relayTuple (tuple, collector);
		}
	}


	// ============================== Abstracted =============================
	/**
	 * Flush window contents
	 */
	protected abstract void flushWindow();


	protected abstract void clear();


	// ============================= Implemented =============================
	@ SuppressWarnings ("rawtypes")
	@ Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector)
	{
		this.collector = collector;
	}


	@ Override
	public Map<String, Object> getComponentConfiguration()
	{
		switch (windowMode)
		{
			case TIME : // Setup time based tumbling window. Configure so a "Tick Tuple" is emitted
						// every flushLimit seconds. Used to trigger the window to flush
				Config conf = new Config ();
				int tickFrequencyInSeconds = windowConstraint;
				conf.put (Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);

				return conf;
			case SIZE :
			default :
				return null;
		}
	}


	@ Override
	public void declareOutputFields(OutputFieldsDeclarer declarer)
	{
		declarer.declare (fields);
	}


	// ========================== Getters & Setters ==========================
	public Fields getFields()
	{
		return fields;
	}


	public OutputCollector getCollector()
	{
		return collector;
	}


	public void setCollector(OutputCollector collector)
	{
		this.collector = collector;
	}


	public WindowMode getWindowMode()
	{
		return windowMode;
	}


	public int getWindowConstraint()
	{
		return windowConstraint;
	}
}