package CDRproject;


import java.util.ArrayList;
import java.util.Map;


import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


/**
 * Emits a tuple every time if an incoming CDRrecord tuple matches a rule
 * - assume taking input stream from tuple relay
 * - this bolt should store incoming rule tuples
 * - this bolt should emit tuples of such that < rule template id, the forwarded CDRrecord object >
 * - this bolt should also forward rule tuples to a specified window bolt to set up the time window
 * 
 * @author Henrik
 */
public class SPSRuleEvaluator implements IRichBolt
{

	// ============================= Attributes ==============================
	private static final long		serialVersionUID	= 1L;

	public static final Fields		fields				= new Fields ("crdRule", "cdrRecord", "matchedTemplateIds");

	protected OutputCollector		collector;

	// the rule lists it maintain for template 1
	private ArrayList<RuleSegment>	rss1;
	// the rule lists it maintain for template 2
	private ArrayList<RuleSegment>	rss2;
	// the rule lists it maintain for template 3
	private ArrayList<RuleSegment>	rss3;

	private String					T1_callType;

	private boolean					eva1;
	private boolean					eva2;
	private boolean					eva3;

	private ArrayList<Integer>		matchedTemplateId;


	public SPSRuleEvaluator ()
	{

		matchedTemplateId = new ArrayList<Integer> ();

		rss1 = new ArrayList<RuleSegment> ();
		rss2 = new ArrayList<RuleSegment> ();
		rss3 = new ArrayList<RuleSegment> ();
	}


	@ Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector)
	{
		// TODO Auto-generated method stub
		this.collector= collector ;
	}


	@ Override
	public void execute(Tuple input)
	{
		// TODO Auto-generated method stub
		// this needs to be checked depending on what the actual input tuple is
		CDRRecord cdr = null;
		RuleSegment rs = null;
		matchedTemplateId.clear ();
//		System.out.println("<====== EVALUATTION ======>");
		// the first field is rule segment
		if (input.getValue (0) != null)
		{
			rs = (RuleSegment) input.getValue (0);
		}

		// the second field is CDR record
		if (input.getValue (1) != null)
		{
			cdr = (CDRRecord) input.getValue (1);
		}

		// set rule for template,
		// currently, only one rule can be active for each template
		if (rs != null)
		{
			switch (rs.getTemplate ())
			{
				case 1 :
					if(rss1.contains(rs)){
						matchedTemplateId.add(rs.getTemplate());
						rs = null;
						rss1.clear();
						break;
					}
					// Just replace previous rule , since we can have only 1 rule per template
					rss1.clear ();
					rss1.add (rs);
					T1_callType = rs.getCallType ();
					break;
				case 2 :
					if(rss2.contains(rs)){
						matchedTemplateId.add(rs.getTemplate());
						rs = null;
						rss2.clear();
						break;
					}
					// Just replace previous rule , since we can have only 1 rule per template
					rss2.clear ();
					rss2.add (rs);
					break;
				case 3 :
					if(rss3.contains(rs)){
						matchedTemplateId.add(rs.getTemplate());
						rs = null;
						rss3.clear();
						break;
					}
					// Just replace previous rule , since we can have only 1 rule per template
					rss3.clear ();
					rss3.add (rs);
					break;
				default :
					break;
			}
		}

		//evaluate CDR record
		if (cdr != null)
		{
			evaluate (cdr);
			if(!matchedTemplateId.isEmpty()){
				collector.emit (new Values (null, cdr, matchedTemplateId));
			}
		}

		if (rs != null)
		{
			matchedTemplateId.add (rs.getTemplate ());
			if(!matchedTemplateId.isEmpty()){
				collector.emit (new Values (rs, null, matchedTemplateId));
			}
		}
		
		if (cdr == null && rs == null){
			if(!matchedTemplateId.isEmpty()){
				collector.emit (new Values (null, null, matchedTemplateId));
			}
		}
	}


	@ Override
	public void cleanup()
	{}


	@ Override
	public void declareOutputFields(OutputFieldsDeclarer declarer)
	{
		declarer.declare (fields);
	}


	@ Override
	public Map<String, Object> getComponentConfiguration()
	{
		return null;
	}


	/**
	 * return true if the CDR tuple matches a rule
	 * 
	 * @param rule
	 * @return
	 */
	public void evaluate(CDRRecord cdrRecord)
	{
		RuleSegment rs = null;
		// Evaluate rules
		for (int i = 0; i < rss1.size (); i++)
		{
			rs = rss1.get (i);
			evaluateT1Rule (rs, cdrRecord);
		}
		for (int i = 0; i < rss2.size (); i++)
		{
			rs = rss2.get (i);
			evaluateT2Rule (rs, cdrRecord);
		}
		for (int i = 0; i < rss3.size (); i++)
		{
			rs = rss3.get (i);
			evaluateT3Rule (rs, cdrRecord);
		}

	}


	private void evaluateT3Rule(RuleSegment r, CDRRecord cdrRecord)
	{
		String causeForTermination = cdrRecord.getCDRType ();
		if (causeForTermination.equals ("MOC"))
			matchedTemplateId.add (r.getTemplate ());
	}


	private void evaluateT2Rule(RuleSegment r, CDRRecord cdrRecord)
	{
		String causeForTermination = cdrRecord.getCauseForTermination ();
		if (causeForTermination.equals ("04"))
			matchedTemplateId.add (r.getTemplate ());
	}


	private void evaluateT1Rule(RuleSegment r, CDRRecord cdrRecord)
	{
		// TODO Auto-generated method stub
		String areaCode = cdrRecord.getLocation ().substring (0, 1);
		if (T1_callType.equals ("INTERNATIONAL"))
		{
			// assume the code begins with 6 is an international number
			if (areaCode.equals ("6"))
			{
				matchedTemplateId.add (r.getTemplate ());
			}
		}
		else
		{ // LOCAL calls
			if ( !areaCode.equals ("6"))
			{
				matchedTemplateId.add (r.getTemplate ());
			}
		}

	}
}