package CDRproject;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;


import CDRproject.Utilities.Frequency;
import CDRproject.Utilities.NullPunctuation;
import CDRproject.Utilities.TupleUtilities;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


public class CustomTopK extends Frequency<String> implements IRichBolt
{
	private static final long	serialVersionUID	= 1L;

	public static final Fields	fields				= new Fields ("field", "frequency");

	protected final String		fieldName;
	protected OutputCollector	collector;

	protected int				accumulationLimit;
	protected int				accumulationCount	= 0;

	protected boolean			active				= false;

	protected int				id;


	public CustomTopK (int k, String fieldName, int id)
	{
		super (k);
		this.fieldName = fieldName;
		this.accumulationLimit = 1;
		this.id = id;
	}


	public CustomTopK (int k, String fieldName, int id, int accumulationLimit)
	{
		super (k);
		this.fieldName = fieldName;
		this.id = id;
		if (accumulationLimit <= 0)
			this.accumulationLimit = 1;
		else
			this.accumulationLimit = accumulationCount;
	}


	@ SuppressWarnings ("rawtypes")
	@ Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector)
	{
		this.collector = collector;
	}


	@ Override
	public void execute(Tuple input)
	{
		if (NullPunctuation.isPunct (input))
		{
			System.out.println ("<====== TOPK ======>");
			if (active)
			{
				accumulationCount++;
				if (accumulationLimit != 0)
				{
					if (accumulationCount % accumulationLimit == 0)
					{
						System.out.printf ("%-60s\t%10s\n", fieldName, "Count");
						System.out.println ("---------------------------------------------------------------------------------------");
						Enumeration<String> keys = this.getFrequencies ().keys ();
						while (keys.hasMoreElements ())
						{
							String key = keys.nextElement ();
							int count = frequencies.get (key);

							System.out.printf ("%-60s\t%10d\n", key, count);
							collector.emit (new Values (key, count));
						}
						clear ();
						NullPunctuation.emitPunctuation (collector, fields.size ());
					}
				}
			}
		}
		else
		{
			// System.out.println("proc");
			if (input.getValue (0) == null && input.getValue (1) == null)
			{
				ArrayList<Integer> rulesMatched = (ArrayList<Integer>) input.getValue (2);

				for (Integer rule : rulesMatched)
				{
					if (rule == this.id)
					{
						active = false;
						clear ();
						accumulationCount = 0;
						break;
					}
				}
			}
			else if (input.getValue (0) != null) // Rule
			{
				active = true;
				final int windowTF = 1 * 60;
				RuleSegment rs = (RuleSegment) input.getValue (0);

				int tFIndex = 0;
				for (String timeframe : RuleSegment.TIMEFRAMES)
				{
					if (timeframe.equals (rs.getTimeframe ()))
						break;

					tFIndex++;
				}

				// Adapt CustomTopK Accumulation limit based on new rule
				int tFSeconds = RuleSegment.timeFrameToSeconds (tFIndex, rs.getValue2 ());
				this.accumulationLimit = tFSeconds / windowTF;

				// reset
				this.accumulationCount = 0;
				clear ();

				TupleUtilities.relayTuple (input, collector);
			}
			else if (input.getValue (1) != null) // Record
			{
				if (active)
				{
					CDRRecord record = (CDRRecord) input.getValue (1);

					String value = record.getRecord ().get (fieldName);
					processElem (value);
				}
			}
		}
	}


	@ Override
	public void cleanup()
	{}


	@ Override
	public void declareOutputFields(OutputFieldsDeclarer declarer)
	{
		declarer.declare (fields);
	}


	@ Override
	public Map<String, Object> getComponentConfiguration()
	{
		// TODO Auto-generated method stub
		return null;
	}
}