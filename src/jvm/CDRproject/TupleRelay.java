package CDRproject;


import java.util.Map;


import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


public class TupleRelay extends BaseRichBolt
{
	private static final long	serialVersionUID	= 1L;

	public final static Fields	fields				= new Fields ("cdrRule", "cdrRecord");

	protected OutputCollector	collector;


	@ SuppressWarnings ("rawtypes")
	@ Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector)
	{
		this.collector = collector;
	}


	@ Override
	public void execute(Tuple input)
	{
		String source = input.getSourceComponent ();

		switch (source)
		{
			case CDRTopology.CDR_RULE_SPOUT :
				collector.emit (new Values ((RuleSegment) input.getValue (0), null));
//				System.out.println ("Relaying rule");
				break;
			case CDRTopology.CDR_VALUE_SPOUT :
				collector.emit (new Values (null, (CDRRecord) input.getValue (0)));
//				System.out.println ("Relaying value");
				break;
			default :
				break;
		}
	}


	@ Override
	public void declareOutputFields(OutputFieldsDeclarer declarer)
	{
		declarer.declare (fields);
	}
}